from collections import defaultdict

def Dijkstra(S, inputs, n):
    matrix = [[a if a != 0 else 100 for a in i] for i in inputs]

    valid = [True] * n
    weight = [100] * n
    weight[S] = 0
    for i in range(n):
        min_weight = 101
        ID_min_weight = -1
        for i in range(n):
            if valid[i] and weight[i] < min_weight:
                min_weight = weight[i]
                ID_min_weight = i
        for i in range(n):
            if weight[ID_min_weight] + matrix[ID_min_weight][i] < weight[i]:
                weight[i] = weight[ID_min_weight] + matrix[ID_min_weight][i]
        valid[ID_min_weight] = False
    return weight

minPath = float('inf')

def calcPathLen(path):


    global minPath

    straightWay = sum([inputs[f][t] for f, t in path[1:]])

    backWay = shortestPaths[path[-1][1]]

    lenPath = straightWay + backWay

    if lenPath < minPath:
        minPath = lenPath

def updateMinPath(u, d, fro_m, visited):

    visited.append((fro_m, u))

    if len(visited) <= 1.4 * n:
        if u == d:
            if len(set([to for fro_m, to in visited])) == n:
                calcPathLen(visited)
        else:
            for i in graph[u]:
                if (u, i) not in visited:
                    updateMinPath(i, d, u, visited)

    visited.remove((fro_m, u))

n = int(input())

inputs = [[int(d) for d in input().split()] for _ in range(n)]

shortestPaths = Dijkstra(0, inputs, n)

graph = defaultdict(list)

for i in range(n):
    for j in range(n):
        if inputs[i][j] != 0:
            graph[i].append(j)

for i in range(1, n):
    updateMinPath(0, i, None, [])

print(minPath)